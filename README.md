What is this
------------

This is a simple application that computes the SHA256 of a given file using an API.

Example
-------

```
$ ./tl-hasher &
[1] 4255
2021/03/26 15:48:51 Listening on port 8080
2021/03/26 15:48:51 Application Started
$ curl -F "file=@/<myfile>" http://localhost:8080/tl-hasher/sha256
{"hash":"a6efffb83fb5469196b8cf0091cf80aefbe5b79b9ffefd0819c46be531636aba","time_taken_ms":2}
```

Build it
----

Requires Golang 1.14

```
$ go build -o tl-hasher
```

########## readme from prem ###################################
############ 1 to 6 question answere are below ##########
Simple go Application EKS using Jenkins pipeline deployment

This is a quick guide to install eks cluster and deploy go application on Kubernetes 2 node cluster setup with managed node.

Assumptions:
•	Jenkin VM running in Linux with required network access to eks cluster
•	Jenkins nodes need to have IAM privileges to query the permissions listed in https://docs.aws.amazon.com/eks/latest/userguide/create-kubeconfig.html
•	Jenkins build/node need to have the following binaries
•	- aws cli
•	- aws-iam-authenticator
•	- kubectl cli
•	- eksctl cli

Build and configure (steps): 


2a) Dockerfile has been devolped and updated in the repo
2b) Jenkinsfile has been devolped which can pull the code from repo and create the docker image & apply the tag and push the same to ECR repo
3) devolped the ymal file to deploy the eks cluster (cluster-autoscaler-spotinstance.yaml) and create the sample go appliction (go-deployment.yaml) which point the service load balancer (ingress-resources.yaml) along with HPA setup inorder to move this setup in prod environment.
4) we can intoruce goldilocks to get the latency of the service which inludes the recommendation of metrics for the pods
5)  created the moniring setup as we need to host the same service in production cluster.
    devopled the sample application with HPA setup so that automatically scalling will happen based up on the load.
    intorduce calico tools for some pod level security 

   
   

final result came as below.
curl -F "file=@/app/var/jenkins/workspace/tl-hasher/README.md" http://a7b183646925b4f50935a52167b599d3-1907925303.eu-west-2.elb.amazonaws.com:80/tl-hasher/sha256
{"hash":"e2144386b901e3f3e3e567e2895225d42f067199b15d53a7e4144d77ae5a37f6","time_taken_ms":0}[root@ip-192-168-68-26 tl-test]#


