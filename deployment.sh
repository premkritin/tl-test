#deploy from eks cluster throughcloud formation
cluster_name=kube-cluster-01
if [ `aws eks list-clusters --region=eu-west-2|jq -c --raw-output | grep -i $cluster_name|wc -l` -gt 0 ]; then
    echo "Cluster Exists, Moving on to next stage"
    exit 0
else
    echo "Creating Cluster...This may take a while"
    eksctl create cluster -f kubernetes/cluster-autoscaler-spotinstance.yaml
fi



