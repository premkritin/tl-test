helm repo add stable https://charts.helm.sh/stable
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
kubectl create ns monitor
helm install prometheus-grafana prometheus-community/kube-prometheus-stack --namespace monitor
